package theme3.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConcurrentModification {

	public static final void main(String args[]) {
		
		List<String> list = new ArrayList<>();
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");		

		//underneath the construct uses Iterable<T> 
		//AbstractList: modCount, checkForModifications().
		for(String e: list) {
			//list.remove(e);
		}
				
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");		

		for(Iterator<String> it = list.iterator(); it.hasNext(); ) {
			if("1".equals(it.next())) {
				it.remove();
			}
		}
		
		list.stream().forEach(System.out::println);
	}
}