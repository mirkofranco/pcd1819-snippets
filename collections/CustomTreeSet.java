package theme3.collections;

import java.util.Set;
import java.util.TreeSet;

final class MyIntWrapper implements Comparable<MyIntWrapper> {

	private final int value;
	
	public MyIntWrapper(int value) {this.value = value;}
	
	@Override
	public int compareTo(MyIntWrapper o) {
		return Integer.compare(o.value, this.value);
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}	
}

public class CustomTreeSet {

	public static final void main(String args[]) {
		Set<MyIntWrapper> oSet = new TreeSet<>();
		oSet.add(new MyIntWrapper(2));	//by contact: add e2, iff foreach e / e==null ? e2==null : !e.equals(e2))
										//declares the use of equals insteadof compareTo.
										//Recall: reccomandation for compareTo conformant to equals.
		oSet.add(new MyIntWrapper(4));
		oSet.add(new MyIntWrapper(1));
		oSet.add(new MyIntWrapper(0));
				
		oSet.add(new MyIntWrapper(0));
		
		for(MyIntWrapper miw: oSet) {
			System.out.println(miw);
		}
	}
	
	
}