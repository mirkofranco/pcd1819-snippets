package theme3.generics;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class A {}
class B extends A {}
class C extends B {}
class D extends B {}

class E extends A {}


public class ClassHierarchy {

	public static void generic1(List<? extends A> type) {
		for(A a: type) {
			System.out.println(a);			
		}

	}
	
	public static final void main(String args[]) {
		List<A> a = Stream.of(new A(), new A()).collect(Collectors.toList());
		List<B> b = Stream.of(new B(), new B()).collect(Collectors.toList());
		List<C> c = Stream.of(new C(), new C()).collect(Collectors.toList());		

		//generic1(a);
		generic1(b);
		generic1(c);
		
		//se permessa covarianza con scrittura
		List<? extends B> listaCov = Stream.of(new C(), new C()).collect(Collectors.toList()); 
		listaCov.add(new D());
	}
}
