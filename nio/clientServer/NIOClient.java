package nio.clientServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
 
public class NIOClient {
 
	public static void main(String[] args) throws IOException, InterruptedException {
 
		InetSocketAddress remoteAddr = new InetSocketAddress("localhost", 1111);
		SocketChannel client = SocketChannel.open(remoteAddr);
 
		log("Connecting to Server on port 1111...");
 
		ArrayList<String> messageList = new ArrayList<String>();
 
		// create a ArrayList with companyName list
		messageList.add("Hi from");
		messageList.add("PCD1819");
		messageList.add("written");
		messageList.add("by");
		messageList.add(Thread.currentThread().getName());
		messageList.add("close");//message used to denote end-of-session
		
		for (String companyName : messageList) {
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			ObjectOutputStream oos = new ObjectOutputStream (baos);
//			oos.writeObject (myObjectToSerialize);
//			oos.flush();
//			channel.write (ByteBuffer.wrap (baos.toByteArray()));

			byte[] message = new String(companyName).getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(message);
			client.write(buffer);
 
			log("sending: " + companyName);
			buffer.clear();
 
			// wait for 2 seconds before sending next message
			Thread.sleep(2000);
		}
		client.close();
	}
 
	private static void log(String str) {
		System.out.println(str);
	}
}
