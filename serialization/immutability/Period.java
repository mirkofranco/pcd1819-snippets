package serialization.immutability;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Period implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;//= 4833281353059335152L;
	private final Date start;
	private final Date end;
	
	public Period(Date start, Date end) {
		this.start = new Date(start.getTime());
		this.end = new Date(end.getTime());
		
		if(this.start.compareTo(this.end) > 0) {
			throw new IllegalArgumentException("Start cannot come after end");
		}
	}
	
	public Date start() {return new Date(start.getTime());}

	public Date end() {return new Date(end.getTime());}
	
	public String toString() {return start + " - " + end;}
	

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		if(this.start.compareTo(this.end) > 0) 
			throw new IllegalArgumentException("Start cannot come after end");
	}

	
	public static final void main(String args[]) {
		
		try(ObjectInputStream reader = new ObjectInputStream(new FileInputStream("invalid.period"))) {
			
			Period p = (Period)reader.readObject();
			System.out.println(p);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
