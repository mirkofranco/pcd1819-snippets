package serialization.immutability;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class InvalidPeriod implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4833281353059335152L;
	private final Date start;
	private final Date end;
	
	public InvalidPeriod(Date start, Date end) {
		this.start = new Date(start.getTime());
		this.end = new Date(end.getTime());
		
//		if(this.start.compareTo(this.end) > 0) {
//			throw new IllegalArgumentException("Start cannot come after end");
//		}
	}
	
	public Date start() {return new Date(start.getTime());}

	public Date end() {return new Date(end.getTime());}
	
	public String toString() {return start + " - " + end;}
	
	public static final void main(String args[]) {
		
		String dateFormat = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

		Date start = null, end = null; 
		try {
			start = simpleDateFormat.parse("2019-09-09");
			end = simpleDateFormat.parse("2018-09-09");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		InvalidPeriod invalidPeriod = new InvalidPeriod(start, end);
		System.out.println(invalidPeriod);
		
		if(Files.exists(Paths.get("invalid.period"))) {
			try {
				Files.delete(Paths.get("invalid.period"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try(ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream("invalid.period"))) {
			
			writer.writeObject(invalidPeriod);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
