package serialization.protobuf;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import theme5.serialization.protobuf.AddressBookProtos.AddressBook;
import theme5.serialization.protobuf.AddressBookProtos.Person;

public class SerializeAddressBook {
  // This function fills in a Person message based on user input.
  static Person PromptForAddress(BufferedReader stdin,
                                 PrintStream stdout) throws IOException {
    Person.Builder person = Person.newBuilder();

    stdout.print("Enter person ID: ");
    person.setId(Integer.valueOf(stdin.readLine()));

    stdout.print("Enter name: ");
    person.setName(stdin.readLine());

    stdout.print("Enter email address (blank for none): ");
    String email = stdin.readLine();
    if (email.length() > 0) {
      person.setEmail(email);
    }

    while (true) {
      stdout.print("Enter a phone number (or leave blank to finish): ");
      String number = stdin.readLine();
      if (number.length() == 0) {
        break;
      }

      Person.PhoneNumber.Builder phoneNumber =
        Person.PhoneNumber.newBuilder().setNumber(number);

      stdout.print("Is this a mobile, home, or work phone? ");
      String type = stdin.readLine();
      if (type.equals("mobile")) {
        phoneNumber.setType(Person.PhoneType.MOBILE);
      } else if (type.equals("home")) {
        phoneNumber.setType(Person.PhoneType.HOME);
      } else if (type.equals("work")) {
        phoneNumber.setType(Person.PhoneType.WORK);
      } else {
        stdout.println("Unknown phone type.  Using default.");
      }

      person.addPhones(phoneNumber);
    }

    return person.build();
  }

  // Main function:  Reads the entire address book from a file,
  //   adds one person based on user input, then writes it back out to the same
  //   file.
  public static void main(String[] args) throws Exception {
	  Files.deleteIfExists(Paths.get("AddressBook.bin"));
	  Path addressBookO = Files.createFile(Paths.get("AddressBook.bin"));

	  AddressBook.Builder addressBook = AddressBook.newBuilder();

	  final int maxPersons = 2;
	  int curr = 0;
	  while(curr < maxPersons) {
		  // Add an address.
		  addressBook.addPeople(
				  PromptForAddress(new BufferedReader(new InputStreamReader(System.in)),
	                       System.out));
		  curr++;
	  }
	  System.out.println("Saving addressBook to file: " + addressBookO.toString());
	  // Write the new address book back to disk.
	  FileOutputStream output = new FileOutputStream(addressBookO.toFile());
	  addressBook.build().writeTo(output);
	  output.close();
  }
}
