package theme3.IOEx;

public class HandleException {

	public static final void main(String args[]) {
		
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

			@Override
			public void uncaughtException(Thread t, Throwable e) {
				System.err.println("An exception of type: " + e.getClass().getName() + " occurred");
			}
		});
		
		System.out.println(5/0);
	}
}
