package theme3.IOEx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class FileIO {

	private final static String codifiedContent = new String("你好");
	
	public static final void main(String args[]) {
		System.out.println("Default Charset=" + Charset.defaultCharset());
		
		File newFile = new File("myFile.ext");
		if(!newFile.exists()) {
			try {
				newFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try (FileWriter writer = new FileWriter(newFile)){		
			System.out.println("OutStream encoding: " + writer.getEncoding());
			System.out.println("Writing default encoded string: " + codifiedContent);
			writer.write(codifiedContent);
			
		} catch (IOException e) {
			//do smth
		}
	
		CharBuffer buffer = CharBuffer.allocate(100);
		StringBuffer tmp = new StringBuffer();
		try(FileReader reader = new FileReader(newFile)){
			System.out.println("InStream encoding: " + reader.getEncoding());
			while(reader.read(buffer)!=-1) {
				buffer.flip();
				tmp.append(buffer.array());
			}
			System.out.println("Read default encoding - txt: " + tmp.toString());			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		tryReadCustomEncoding(newFile);
	}
	
	public static void tryReadCustomEncoding(File file) {
		CharBuffer buffer = CharBuffer.allocate(100);
		StringBuffer tmp = new StringBuffer();
		try(InputStreamReader reader = new InputStreamReader(new FileInputStream(file), "Cp1250")){
			System.out.println("InStream encoding: " + reader.getEncoding());
			while(reader.read(buffer)!=-1) {
				buffer.flip();
				tmp.append(buffer.array());
			}
			System.out.println("Read custom txt: " + tmp.toString());			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
