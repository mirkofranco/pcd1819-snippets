package lec1;

class B implements Cloneable {
	
	private int[] b;
	public B() { b = new int[] {1,2};}
	public int[] getB() {return b;}
	

	@Override
	public Object clone() {
		B copy = null;
		try {
			copy = (B)super.clone();
			System.out.println("B::clone() with RT classType: " + copy.getClass().getName());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		copy.b = (int[]) this.b.clone();
		
		return copy;
	}
}

public class A extends B {
	
	private int a;
	public A() {a = 1;}
	public int getA() {return a;}
	
	@Override
	public Object clone() {
		A copy = null;
		copy = (A)super.clone();
		System.out.println("A::clone() with RT classType: " + copy.getClass().getName());		
		return copy;
	}
	
	
	public static void main(String args[]) {
		
		A a = new A();
		A copy = (A)a.clone();
		
		System.out.println(a == copy);
		System.out.println(a.getA() == copy.getA());
		System.out.println(a.getB() == copy.getB());		
	}
}